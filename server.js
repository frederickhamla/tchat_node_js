import express from 'express'
import DataStore from 'nedb'

const hostname = '127.0.0.1';
const port = 3000;
const app = express()

// DB
const database = new DataStore({ filename: "user" })

const myObj = {
    'name': 'Frédérick',
    'lastname': 'Khamla'
}

app.get('/', (req, res) => {
    res.status(200).json(myObj);
});

app.listen(port, hostname, () => {
    console.log('coucou')
})

// const server = http.createServer((req, res) => {
//     res.statusCode = 200;
//     res.setHeader('Content-Type', 'text/plain');
//     res.end('Test');
// });

// server.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });